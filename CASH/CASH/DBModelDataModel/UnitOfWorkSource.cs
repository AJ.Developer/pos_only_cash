﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.DesignTime;
using DevExpress.Mvvm.DataModel.EF6;
using CASH.DataLayer;
using System;
using System.Collections;
using System.Linq;

namespace CASH.DBModelDataModel
{

    public static class UnitOfWorkSource
    {

        public static IUnitOfWorkFactory<IDBModelUnitOfWork> GetUnitOfWorkFactory()
        {
            return GetUnitOfWorkFactory(ViewModelBase.IsInDesignMode);
        }

        public static IUnitOfWorkFactory<IDBModelUnitOfWork> GetUnitOfWorkFactory(bool isInDesignTime)
        {
            if (isInDesignTime)
                return new DesignTimeUnitOfWorkFactory<IDBModelUnitOfWork>(() => new DBModelDesignTimeUnitOfWork());
            return new DbUnitOfWorkFactory<IDBModelUnitOfWork>(() => new DBModelUnitOfWork(() => new DBModel()));
        }
        public static bool IsAdmin { get; set; }
        public static User CurrentUser { get; set; }
    }
}