﻿using DevExpress.Mvvm.DataModel;
using CASH.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CASH.DBModelDataModel {

    /// <summary>
    /// IDBModelUnitOfWork extends the IUnitOfWork interface with repositories representing specific entities.
    /// </summary>
    public interface IDBModelUnitOfWork : IUnitOfWork {

        IRepository<Configuration, int> Configurations { get; }
      
        /// <summary>
        /// The Invoice entities repository.
        /// </summary>
		IRepository<Invoice, long> Invoices { get; }
        
        /// <summary>
        /// The InvoiceItem entities repository.
        /// </summary>
		IRepository<InvoiceItem, long> InvoiceItems { get; }
        
        
        /// <summary>
        /// The StockItem entities repository.
        /// </summary>
		IRepository<StockItem, long> StockItems { get; }
        

        
        /// <summary>
        /// The User entities repository.
        /// </summary>
		IRepository<User, long> Users { get; }
    }
}
