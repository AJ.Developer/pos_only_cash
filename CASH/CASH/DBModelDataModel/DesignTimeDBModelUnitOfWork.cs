﻿using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.DesignTime;
using CASH.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CASH.DBModelDataModel
{

    /// <summary>
    /// A DBModelDesignTimeUnitOfWork instance that represents the design-time implementation of the IDBModelUnitOfWork interface.
    /// </summary>
    public class DBModelDesignTimeUnitOfWork : DesignTimeUnitOfWork, IDBModelUnitOfWork
    {

        /// <summary>
        /// Initializes a new instance of the DBModelDesignTimeUnitOfWork class.
        /// </summary>
        public DBModelDesignTimeUnitOfWork()
        {
        }


        IRepository<Invoice, long> IDBModelUnitOfWork.Invoices
        {
            get { return GetRepository((Invoice x) => x.InvoiceID); }
        }
        IRepository<Configuration, int> IDBModelUnitOfWork.Configurations
        {
            get { return GetRepository((Configuration x) => x.ID); }
        }
        IRepository<InvoiceItem, long> IDBModelUnitOfWork.InvoiceItems
        {
            get { return GetRepository((InvoiceItem x) => x.InvoiceItemID); }
        }



        IRepository<StockItem, long> IDBModelUnitOfWork.StockItems
        {
            get { return GetRepository((StockItem x) => x.StockItemID); }
        }



        IRepository<User, long> IDBModelUnitOfWork.Users
        {
            get { return GetRepository((User x) => x.UserID); }
        }
    }
}
