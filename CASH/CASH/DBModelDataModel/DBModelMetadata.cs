﻿using DevExpress.Mvvm.DataAnnotations;
using CASH.DataLayer;
using CASH.Localization;

namespace CASH.DBModelDataModel
{

    public class DBModelMetadataProvider
    {

        public static void BuildMetadata(MetadataBuilder<Invoice> builder)
        {
            builder.DisplayName(DBModelResources.Invoice);
            builder.Property(x => x.InvoiceID).DisplayName(DBModelResources.Invoice_InvoiceID);
        }
        public static void BuildMetadata(MetadataBuilder<InvoiceItem> builder)
        {
            builder.DisplayName(DBModelResources.InvoiceItem);
            builder.Property(x => x.InvoiceItemID).DisplayName(DBModelResources.InvoiceItem_InvoiceItemID);
            builder.Property(x => x.Quantity).DisplayName(DBModelResources.InvoiceItem_Quantity);
            builder.Property(x => x.Invoice).DisplayName(DBModelResources.InvoiceItem_Invoice);
            builder.Property(x => x.StockItem).DisplayName(DBModelResources.InvoiceItem_StockItem);
        }

        public static void BuildMetadata(MetadataBuilder<StockItem> builder)
        {
            builder.DisplayName(DBModelResources.StockItem);
            builder.Property(x => x.StockItemID).DisplayName(DBModelResources.StockItem_StockItemID);
            builder.Property(x => x.Name).DisplayName(DBModelResources.StockItem_Name);
            builder.Property(x => x.SalePrice).DisplayName(DBModelResources.StockItem_SalePrice);
            builder.Property(x => x.Quantity).DisplayName(DBModelResources.StockItem_Quantity);
        }

        public static void BuildMetadata(MetadataBuilder<User> builder)
        {
            builder.DisplayName(DBModelResources.User);
            builder.Property(x => x.UserID).DisplayName(DBModelResources.User_UserID);
            builder.Property(x => x.Username).DisplayName(DBModelResources.User_Username);
            builder.Property(x => x.Password).DisplayName(DBModelResources.User_Password);
            builder.Property(x => x.IsAdmin).DisplayName(DBModelResources.User_IsAdmin);
        }

    }
}