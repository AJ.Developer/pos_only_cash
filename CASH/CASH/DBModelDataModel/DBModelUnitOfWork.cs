﻿using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.EF6;
using CASH.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CASH.DBModelDataModel
{

    /// <summary>
    /// A DBModelUnitOfWork instance that represents the run-time implementation of the IDBModelUnitOfWork interface.
    /// </summary>
    public class DBModelUnitOfWork : DbUnitOfWork<DBModel>, IDBModelUnitOfWork
    {

        public DBModelUnitOfWork(Func<DBModel> contextFactory)
            : base(contextFactory)
        {
        }

        IRepository<Invoice, long> IDBModelUnitOfWork.Invoices
        {
            get { return GetRepository(x => x.Set<Invoice>(), (Invoice x) => x.InvoiceID); }
        }
        IRepository<Configuration, int> IDBModelUnitOfWork.Configurations
        {
            get { return GetRepository(x => x.Set<Configuration>(), (Configuration x) => x.ID); }
        }
        IRepository<InvoiceItem, long> IDBModelUnitOfWork.InvoiceItems
        {
            get { return GetRepository(x => x.Set<InvoiceItem>(), (InvoiceItem x) => x.InvoiceItemID); }
        }



        IRepository<StockItem, long> IDBModelUnitOfWork.StockItems
        {
            get { return GetRepository(x => x.Set<StockItem>(), (StockItem x) => x.StockItemID); }
        }

        IRepository<User, long> IDBModelUnitOfWork.Users
        {
            get { return GetRepository(x => x.Set<User>(), (User x) => x.UserID); }
        }
    }
}
