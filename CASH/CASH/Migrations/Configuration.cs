namespace CASH.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<CASH.DataLayer.DBModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "CASH.DBModel";
        }

        protected override void Seed(CASH.DataLayer.DBModel context)
        {
            //This method will be called after migrating to the latest version.

            //You can use the DbSet<T>.AddOrUpdate() helper extension method
            //to avoid creating duplicate seed data.E.g.

            //context.Users.AddOrUpdate(
            //  new DataLayer.User { Username = "Admin" ,Password = "Admin",IsAdmin = true}
            //);

        }
    }
}
