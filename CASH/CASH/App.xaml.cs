﻿using DevExpress.Xpf.Core;
using CASH.DataLayer;
using CASH.ViewModels;
using CASH.Views.Login;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace CASH
{

    public partial class App : Application
    {
        void App_Startup(object sender, StartupEventArgs e)
        {
            try
            {

                ShutdownMode = ShutdownMode.OnLastWindowClose;
                if (DateTime.Now.Year > 2020)
                    this.Shutdown();
                ApplicationThemeHelper.ApplicationThemeName = Theme.HybridApp.Name;
         
                using (var context = new DBModel())
                {
                    var Users = context.Users.ToArray();
                    var Invoices = context.Invoices.ToArray();
                    var InvoiceItems = context.InvoiceItems.ToArray();
                    var StockItems = context.StockItems.ToArray();

                    if ( StockItems.Where(x => x.IsPrimary).FirstOrDefault() == null)
                    {
                        StockItem stockItem = new StockItem { ForeGround = "White" , BackGround = "#FF1F497D", Name = "Others" , IsPrimary = true ,IsAvailable=true};                  
                        context.StockItems.Add(stockItem);
                        context.SaveChanges();
                    }
                }
                // Create main application window, starting minimized if specified
                var login = new LoginCollectionView();
                LoginCollectionViewModel loginVM = LoginCollectionViewModel.Create();
                loginVM.LoginCompleted += (a, args) =>
                {                  
                    MainWindow main = new MainWindow();
                    main.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    main.WindowState = WindowState.Maximized;
                    main.Show();
                    login.Close();
                };

                login.DataContext = loginVM;
                login.ShowDialog();
                CultureInfo ci = new CultureInfo("ar-LB");
                System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Current.ShutdownMode = ShutdownMode.OnLastWindowClose;
                Current.Shutdown(0);
            }

        }
    }
}
