namespace CASH.DataLayer
{
    using System.Data.Entity;
    public class DBModel : DbContext
    {
        public DBModel()
            : base(nameOrConnectionString: "Express")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DBModel, CASH.Migrations.Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {                       
            base.OnModelCreating(modelBuilder);
        }
        public virtual DbSet<Configuration> Configurations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceItem> InvoiceItems { get; set; }
        public DbSet<StockItem> StockItems { get; set; }
    }
}