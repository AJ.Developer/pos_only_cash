﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CASH.DataLayer
{
    [Table("InvoiceItems")]
    public class InvoiceItem
    {
        [Key]
        public long InvoiceItemID { get; set; }

        [Required]
        public int Quantity { get; set; }

        public int MyQuantity { get; set; }

        //StockItem
        public long StockItemID { get; set; }
        [ForeignKey("StockItemID")]
        public virtual StockItem StockItem { get; set; }

        //Invoice
        public long InvoiceID { get; set; }
        [ForeignKey("InvoiceID")]
        public virtual Invoice Invoice { get; set; }

        public override string ToString()
        {
            try
            {
                return Quantity + " " + StockItem.Name;
            }
            catch (Exception)
            {
                return "";
            }
        }
        public virtual decimal UnitPrice { get; set; }
        //{
        //    get
        //    {
        //        if (StockItem != null)
        //            return StockItem.SalePrice;
        //        else
        //            return 0;
        //    }
        //}
        [NotMapped]
        public virtual decimal TotalPrice
        {
            get
            {
                if (StockItem != null && !StockItem.IsCustom)
                    return UnitPrice * Quantity;
                else
                    if (StockItem != null && StockItem.IsCustom)
                    return UnitPrice * Quantity / 1000;
                else
                    return 0;

            }
        }
    }
}
