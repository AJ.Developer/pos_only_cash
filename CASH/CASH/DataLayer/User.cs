﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CASH.DataLayer
{
    [Table("Users")]
    public class User : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Username")&& string.IsNullOrEmpty(this.Username))
                        return "يجب إدخال اسم المستخدم";
                return string.Empty;
            }
        }

        [Key]
        public long UserID { get; set; }

        [Required]
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public virtual ICollection<Invoice> Invoice { get; set; }
        [NotMapped]
        public string Error => "";
    }
}
