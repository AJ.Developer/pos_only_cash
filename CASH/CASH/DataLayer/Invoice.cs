﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CASH.DataLayer
{
    [Table("Invoices")]
    public class Invoice
    {
        [Key]
        public long InvoiceID { get; set; }
        public Invoice()
        {
            InvoiceItems = new HashSet<InvoiceItem>();
        }
        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        public DateTime? Date { get; set; }

        [NotMapped]
        public string FormattedDate
        {
            get
            {
                if (Date != null)
                    return Date.Value.ToString("ddd, dd MMM hh:mm tt");
                else
                    return "";
            }
        }
        [NotMapped]
        public string FilterDate
        {
            get
            {
                if (Date != null)
                    return Date.Value.ToString("ddd, dd MMM") + " ";
                else
                    return "";
            }
        }


        //User  
        public long UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual User User { get; set; }

        [NotMapped]
        public decimal TotalAmount
        {
            get
            {
                if (this.InvoiceItems != null)
                {
                    decimal amount = 0;
                    foreach (InvoiceItem item in InvoiceItems)
                    {
                        if (item.StockItem.IsCustom)
                            amount += ((item.Quantity * item.UnitPrice)/1000);
                        else
                            amount += (item.Quantity * item.UnitPrice);
                    }
                    return amount;
                }
                else
                    return 0;
            }
        }
       

    }
}
