﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CASH.DataLayer
{
    [Table("StockItems")]
    public class StockItem : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                DBModel dBModel = new DBModel();
                switch (columnName)
                {
                    case "Name":
                        if (string.IsNullOrEmpty(this.Name))
                            return "يجب ادخال الاسم";
                        if (dBModel.StockItems.Where(x => x.StockItemID != this.StockItemID && x.Name == this.Name).FirstOrDefault() != null)
                            return "هذا الصّنف موجود";
                        break;
                    case "SalePrice":
                        if (this.SalePrice < 250 && !IsPrimary)
                            return "السّعر الأدنى هو 250 ليرة لبنانيّة";
                        break;
                    case "Quantity":
                    case "MinimumQuantity":
                        if (this.Quantity < this.MinimumQuantity && !IsPrimary)
                        {
                            Error = "الكمّيّة أقل من الحد الأنى";
                            return "الكمّيّة أقل من الحد الأنى";
                        }
                        Error = "";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }

        [Key]
        public long StockItemID { get; set; }
        [Required]
        public virtual string Name { get; set; }
        public virtual string Barcode { get; set; }
        [Required]
        public decimal SalePrice { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public int MinimumQuantity { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsCustom { get; set; }
        public bool IsPrimary { get; set; }
        public string BackGround { get; set; }

        public string ForeGround { get; set; }

        [NotMapped]
        public string Error { get; set; } = "";
    }
}
