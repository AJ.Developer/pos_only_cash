﻿using CASH.DataLayer;
using System;
using System.Globalization;
using System.Windows.Data;
using DevExpress.Utils;
using CASH.ViewModels;

namespace CASH.Resources
{

    public class SpinEditValueToInt : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return int.Parse(value.ToString());
            }

            return 0;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return int.Parse(value.ToString());
            }

            return 0;
        }
    }

    public class QuantityConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                //var uri = AssemblyHelper.GetResourceUri(typeof(ImageConverter).Assembly, value.ToString());
                //return new System.Windows.Media.Imaging.BitmapImage(uri);
            }
            return null;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ImageConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var uri = AssemblyHelper.GetResourceUri(typeof(ImageConverter).Assembly, value.ToString());
                return new System.Windows.Media.Imaging.BitmapImage(uri);
            }
            return null;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
