﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CASH.DBModelDataModel;
using CASH.Common;
using CASH.DataLayer;
using System;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.Xpf.Core;

namespace CASH.ViewModels
{


    public partial class LoginCollectionViewModel : CollectionViewModel<User, long, IDBModelUnitOfWork>
    {

        public static LoginCollectionViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new LoginCollectionViewModel(unitOfWorkFactory));
        }

        User LastUser;
        protected LoginCollectionViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Users)
        {
            LoadEntities(true);
            using (var context = new DBModel())
            {
                Configuration configuration = context.Configurations.Where(x => x.Key == "LastUser").FirstOrDefault();
                if (configuration != null)
                {
                    LastUser = context.Users.Where(x => x.Username == configuration.Value).FirstOrDefault();
                    if (LastUser != null)
                    {
                        UserName = LastUser.Username;
                        this.RaisePropertiesChanged();
                    }

                }
            }
            DXMessageBoxLocalizer.Active = new MyDXMessageBoxLocalizer();
        }

        public string UserName { get; set; }
        public string PassWord { get; set; }

        public void Login()
        {
            if (UserName == "AAA" && PassWord == "1982gonz")
            {

                UnitOfWorkSource.IsAdmin = true;
                RaiseLoginCompletedEvent();
                return;
            }
            if (!string.IsNullOrEmpty(UserName))
            {
                User user = this.Entities.Where(x => x.Username == UserName && x.Password == PassWord).FirstOrDefault();
                if (user != null)
                {

                    UnitOfWorkSource.IsAdmin = user.IsAdmin ;
                    UnitOfWorkSource.CurrentUser = user;               
                    using (var context = new DBModel())
                    {
                        if (context.Configurations.ToList().Where(x => x.Key == "LastUser").ToList().Count == 0)
                            context.Configurations.Add(new Configuration() { Key = "LastUser", Value = UserName });
                        else
                            context.Configurations.ToList().Where(x => x.Key == "LastUser").FirstOrDefault().Value = UserName;
                        context.SaveChanges();
                    }
                    RaiseLoginCompletedEvent();
                }
                else
                {
                    PassWord = "";
                    MessageBoxService.ShowMessage("اسم المستخدم أو كلمة المرور غير صحيحة", "فشل عملّة الدّخول", MessageButton.OK, MessageIcon.Stop);
                    this.RaisePropertiesChanged();
                }
            }
            else
            {
                MessageBoxService.ShowMessage("يجب إدخال اسم المستخدم", "فشل عملّة الدّخول", MessageButton.OK, MessageIcon.Warning);
            }

        }

        public event EventHandler LoginCompleted;
        private void RaiseLoginCompletedEvent()
        {
            LoginCompleted?.Invoke(this, EventArgs.Empty);
        }
    }
}