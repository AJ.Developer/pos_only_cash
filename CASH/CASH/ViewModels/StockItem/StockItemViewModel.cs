﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CASH.DBModelDataModel;
using CASH.Common;
using CASH.DataLayer;

namespace CASH.ViewModels
{


    public partial class StockItemViewModel : SingleObjectViewModel<StockItem, long, IDBModelUnitOfWork>
    {


        public static StockItemViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new StockItemViewModel(unitOfWorkFactory));
        }


        protected StockItemViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.StockItems, x => x.StockItemID)
        {
        }
        public override string EntityDisplayName => "الصّنف";

        public override void OnLoaded()
        {
            base.OnLoaded();
            RefreshLookUpCollections(true);
        }
        public void ShowChanges()
        {
            if (this.Entity != null && !string.IsNullOrEmpty(this.Entity.Barcode))
                this.Entity.IsCustom = false;
            this.RaisePropertiesChanged();
        }
        public bool DontHaveBarcode
        {
            get
            {
                return this.Entity != null && string.IsNullOrEmpty(this.Entity.Barcode);
            }
        }
        public StockItem CreateEntityFromOutside()
        {
            StockItem StockItem = base.CreateEntity();
            StockItem.BackGround = "#FF1F497D";
            StockItem.ForeGround = "White";
            StockItem.IsAvailable = true;
            return StockItem;
        }
        protected override StockItem CreateEntity()
        {
            StockItem StockItem = base.CreateEntity();
            StockItem.BackGround = "#FF1F497D";
            StockItem.ForeGround = "White";
            StockItem.IsAvailable = true;
            return StockItem;
        }


    }
}
