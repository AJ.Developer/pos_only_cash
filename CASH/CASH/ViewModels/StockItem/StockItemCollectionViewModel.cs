﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CASH.DBModelDataModel;
using CASH.Common;
using CASH.DataLayer;

namespace CASH.ViewModels
{


    public partial class StockItemCollectionViewModel : CollectionViewModel<StockItem, long, IDBModelUnitOfWork>
    {


        public static StockItemCollectionViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new StockItemCollectionViewModel(unitOfWorkFactory));
        }

        protected StockItemCollectionViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.StockItems)
        {
        }
        public override bool CanDelete(StockItem projectionEntity)
        {
            return projectionEntity != null && !projectionEntity.IsPrimary;
        }
        public override bool CanEdit(StockItem projectionEntity)
        {
            return projectionEntity != null && !projectionEntity.IsPrimary;
        }
    }
}