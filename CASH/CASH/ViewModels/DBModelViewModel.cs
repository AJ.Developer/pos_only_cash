﻿using System;
using System.Linq;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.ViewModel;
using CASH.Localization;
using CASH.DBModelDataModel;
using CASH.Views.Login;
using System.Windows;
using DevExpress.Xpf.Core;

namespace CASH.ViewModels
{
    /// <summary>
    /// Represents the root POCO view model for the DBModel data model.
    /// </summary>
    public partial class DBModelViewModel : DocumentsViewModel<DBModelModuleDescription, IDBModelUnitOfWork>
    {

        const string Sales = "المبيعات";


        const string Persons = "الأفراد";


        INavigationService NavigationService { get { return this.GetService<INavigationService>(); } }

        public static DBModelViewModel Create()
        {
            return ViewModelSource.Create(() => new DBModelViewModel());
        }
        public override void SaveLogicalLayout() { }
        public override bool RestoreLogicalLayout()
        {
            return false;
        }

        public override void OnLoaded(DBModelModuleDescription module)
        {
            base.OnLoaded(module);
            //SelectedModule = Modules.Where(x => x.ModuleTitle == DBModelResources.InvoicePlural).FirstOrDefault();
        }

        static DBModelViewModel()
        {
            MetadataLocator.Default = MetadataLocator.Create().AddMetadata<DBModelMetadataProvider>();
        }

        protected DBModelViewModel()
            : base(UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }

        protected override DBModelModuleDescription[] CreateModules()
        {
            if (UnitOfWorkSource.IsAdmin)
                return new DBModelModuleDescription[] {
                new DBModelModuleDescription("الفواتير", "InvoiceCollectionView", Sales, GetPeekCollectionViewModelFactory(x => x.Invoices)),
                new DBModelModuleDescription("المخزن", "StockItemCollectionView", Sales, GetPeekCollectionViewModelFactory(x => x.StockItems)),
                new DBModelModuleDescription("المستخدمون", "UserCollectionView", Persons, GetPeekCollectionViewModelFactory(x => x.Users)),
                new DBModelModuleDescription("تسجيل خروج", "", "",null)
            };
            else
                return new DBModelModuleDescription[] {
                      new DBModelModuleDescription("الفواتير", "InvoiceCollectionView", Sales, GetPeekCollectionViewModelFactory(x => x.Invoices)),
                      new DBModelModuleDescription("تسجيل خروج", "", "",null)
            };
        }

        public override DBModelModuleDescription DefaultModule
        {
            get
            {
                return Modules.Where(x => x.ModuleTitle == "الفواتير").FirstOrDefault();
            }
        }
        protected override void OnActiveModuleChanged(DBModelModuleDescription oldModule)
        {
            base.OnActiveModuleChanged(oldModule);
            if (ActiveModule == null)
                return;
            if (ActiveModule != null && NavigationService != null)
            {
                NavigationService.ClearNavigationHistory();
            }
            if (!Modules.Contains(ActiveModule))
            {
                ActiveModule = Modules[0];
                NavigationService.ClearNavigationHistory();
            }
            if (ActiveModule != null && ActiveModule.ModuleTitle == "تسجيل خروج")
            {
                // Create main application window, starting minimized if specified
                App.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                while (App.Current.Windows.Count > 0)
                    App.Current.Windows[0].Close();
                var login = new LoginCollectionView();
                LoginCollectionViewModel loginVM = LoginCollectionViewModel.Create();
                loginVM.LoginCompleted += (a, args) =>
                {

                    MainWindow main = new MainWindow();
                    main.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    main.WindowState = WindowState.Maximized;
                    main.Show();
                    login.Close();
                    App.Current.ShutdownMode = ShutdownMode.OnLastWindowClose;

                };

                login.DataContext = loginVM;
                login.ShowDialog();
            }

        }
    }

    public partial class DBModelModuleDescription : ModuleDescription<DBModelModuleDescription>
    {
        public DBModelModuleDescription(string title, string documentType, string group, Func<DBModelModuleDescription, object> peekCollectionViewModelFactory = null)
          : base(title, documentType, group, peekCollectionViewModelFactory)
        {
            if(title == "الفواتير")
                ImageSource = new Uri(string.Format(@"pack://application:,,,/CASH;component/Resources/Images/Invoices.png"));
            if(title == "المخزن")
                ImageSource = new Uri(string.Format(@"pack://application:,,,/CASH;component/Resources/Images/stock Items.png"));
            if (title == "المستخدمون")
                ImageSource = new Uri(string.Format(@"pack://application:,,,/CASH;component/Resources/Images/Users.png"));
            if (title == "تسجيل خروج")
                ImageSource = new Uri(string.Format(@"pack://application:,,,/CASH;component/Resources/Images/LogOut.png"));
            IsModuleVisible = true;
        }

        public Uri ImageSource { get; private set; }

        public bool IsModuleVisible { get; private set; }

    }
}