﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CASH.DBModelDataModel;
using CASH.Common;
using CASH.DataLayer;

namespace CASH.ViewModels
{


    public partial class InvoiceItemViewModel : SingleObjectViewModel<InvoiceItem, long, IDBModelUnitOfWork>
    {


        public static InvoiceItemViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new InvoiceItemViewModel(unitOfWorkFactory));
        }

        protected InvoiceItemViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.InvoiceItems, x => x.InvoiceItemID)
        {
        }
        public void CustomAdd(InvoiceItem InvoiceItem)
        {
            this.Entity = InvoiceItem;
            base.Save();
        }
        public override string EntityDisplayName => "الفاتورة";


        public IEntitiesViewModel<Invoice> LookUpInvoices
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceItemViewModel x) => x.LookUpInvoices,
                    getRepositoryFunc: x => x.Invoices);
            }
        }

        public IEntitiesViewModel<StockItem> LookUpStockItems
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceItemViewModel x) => x.LookUpStockItems,
                    getRepositoryFunc: x => x.StockItems);
            }
        }

    }
}
