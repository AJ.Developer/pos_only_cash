﻿using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CASH.DBModelDataModel;
using CASH.Common;
using CASH.DataLayer;

namespace CASH.ViewModels
{


    public partial class InvoiceItemCollectionViewModel : CollectionViewModel<InvoiceItem, long, IDBModelUnitOfWork>
    {


        public static InvoiceItemCollectionViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new InvoiceItemCollectionViewModel(unitOfWorkFactory));
        }

        protected InvoiceItemCollectionViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.InvoiceItems)
        {
        }
    }
}