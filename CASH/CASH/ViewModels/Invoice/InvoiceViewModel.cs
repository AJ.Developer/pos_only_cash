﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using CASH.DBModelDataModel;
using CASH.Common;
using CASH.DataLayer;
using System.Drawing.Printing;
using System.Text;
using System.Drawing;
using DevExpress.Xpf.WindowsUI;
using System.Management;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using System.ComponentModel;
using System.Windows;

namespace CASH.ViewModels
{


    public partial class InvoiceViewModel : SingleObjectViewModel<Invoice, long, IDBModelUnitOfWork>
    {

        public virtual ObservableCollection<StockItem> StockItems { get; set; }
        public static InvoiceViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new InvoiceViewModel(unitOfWorkFactory));
        }

        protected InvoiceViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Invoices, x => x.InvoiceID)
        {       
            //StockItems = new ObservableCollection<StockItem>(UnitOfWork.StockItems.Where(x => string.IsNullOrEmpty(x.Barcode))?.ToList());
            StockItems = new ObservableCollection<StockItem>(UnitOfWork.StockItems.Where(x => x.IsAvailable)?.ToList());
        }
        public override string EntityDisplayName => "الفاتورة";
        protected override bool TryClose()
        {
            return true;
        }
        public void UpdateItemQuantity(InvoiceItem InvoiceItem)
        {
            if (!InvoiceItem.StockItem.IsPrimary)
            {
                if (!InvoiceItem.StockItem.IsCustom)
                {
                    int Qdiff = InvoiceItem.MyQuantity - InvoiceItem.Quantity;
                    this.Entity.InvoiceItems.Where(x => x.InvoiceItemID == InvoiceItem.InvoiceItemID).FirstOrDefault().Quantity = InvoiceItem.Quantity;
                    InvoiceItem.StockItem.Quantity += Qdiff;
                    InvoiceItem.MyQuantity = InvoiceItem.Quantity;
                }         
            }
            else
            {
                InvoiceItem.Quantity = 1;
            }

            base.Save();
        }
        protected override void OnBeforeEntitySaved(long primaryKey, Invoice entity, bool isNewEntity)
        {
            try
            {
                if (isNewEntity || this.Entity.Date == null)
                    this.Entity.Date = DateTime.Now;

                if (this.Entity.UserID == 0)
                    this.Entity.UserID = UnitOfWorkSource.CurrentUser.UserID;

                base.OnBeforeEntitySaved(primaryKey, entity, isNewEntity);

            }
            catch (Exception)
            {

            }
        }
        public virtual string Barcode { get; set; }
        public void DealWithBarcode()
        {
            if (!string.IsNullOrEmpty(Barcode))
            {
                if (UnitOfWork.StockItems.Where(x => x.Barcode == Barcode).FirstOrDefault() != null)
                {
                    AddStockItemToInvoice(UnitOfWork.StockItems.Where(x => x.Barcode == Barcode).FirstOrDefault().StockItemID);
                    Barcode = "";
                    this.RaisePropertiesChanged();
                }
            }

        }
        [ServiceProperty(Key = "Dialog1")]
        protected virtual IDialogService Dialog1Service { get { return null; } }
        [ServiceProperty(Key = "Primary")]

        protected virtual IDialogService PrimaryService { get { return null; } }

        public void Dialog1()
        {
            var okCommand = new UICommand
            {
                Caption = "تمّ",
                IsCancel = false,
                IsDefault = true,
                //   you can add an Action here  
                Command = new DelegateCommand<CancelEventArgs>(x => { }, true)
            };

            var cancelCommand = new UICommand()
            {
                Id = MessageBoxResult.Cancel,
                Caption = "إلغاء",
                IsCancel = true,
                IsDefault = false
            };

            var result = Dialog1Service.ShowDialog(new List<UICommand> { okCommand, cancelCommand }, "Dialog 1 View", null);

            if (result == null || result.IsCancel)
                MessageBox.Show("إلغاء");

            if (result.IsDefault)
                MessageBox.Show("تمّ");
        }
        public void Primary()
        {
            var okCommand = new UICommand
            {
                Caption = "تمّ",
                IsCancel = false,
                IsDefault = true,
                //   you can add an Action here  
                Command = new DelegateCommand<CancelEventArgs>(x => { }, true)
            };

            var cancelCommand = new UICommand()
            {
                Id = MessageBoxResult.Cancel,
                Caption = "إلغاء",
                IsCancel = true,
                IsDefault = false
            };

            var result = PrimaryService.ShowDialog(new List<UICommand> { okCommand, cancelCommand }, "Primary View", null);

            if (result == null || result.IsCancel)
                MessageBox.Show("إلغاء");

            if (result.IsDefault)
                MessageBox.Show("تمّ");
        }
        public void AddStockItemToInvoice(long id)
        {
            try
            {
                if (this.Entity != null)
                {
                    if (this.Entity.InvoiceID == 0)
                        base.Save();
                    if (UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault() != null && UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault().IsCustom)
                    {

                        var okCommand = new UICommand
                        {
                            Caption = "تمّ",
                            IsCancel = false,
                            IsDefault = true,
                            //   you can add an Action here  
                            Command = new DelegateCommand<CancelEventArgs>(x => { }, true)
                        };

                        var cancelCommand = new UICommand()
                        {
                            Id = MessageBoxResult.Cancel,
                            Caption = "إلغاء",
                            IsCancel = true,
                            IsDefault = false
                        };
                        CustomViewModel customViewModel = CustomViewModel.Create();
                        customViewModel.MyTitle = UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault().Name;
                        customViewModel.Value = 0;
                        var result = Dialog1Service.ShowDialog(new List<UICommand> { okCommand, cancelCommand }, UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault().Name, customViewModel);

                        if (result.IsDefault)
                        {
                            InvoiceItem invoiceItem = this.Entity.InvoiceItems.Where(x => x.StockItemID == id).FirstOrDefault();
                            if (invoiceItem == null)
                            {
                                invoiceItem = new InvoiceItem();
                                invoiceItem.Quantity = customViewModel.Value;
                                invoiceItem.MyQuantity = customViewModel.Value;
                                invoiceItem.StockItemID = id;
                                invoiceItem.StockItem = UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault();
                                invoiceItem.UnitPrice = invoiceItem.StockItem.SalePrice;
                                this.Entity.InvoiceItems.Add(invoiceItem);
                            }
                            else
                            {
                                invoiceItem.Quantity += customViewModel.Value;
                                invoiceItem.MyQuantity = invoiceItem.Quantity;

                            }                 
                            base.Save();
                        }
                    }
                    else
                    {
                        if (UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault() != null && UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault().IsPrimary)
                        {

                            var okCommand = new UICommand
                            {
                                Caption = "تمّ",
                                IsCancel = false,
                                IsDefault = true,
                                //   you can add an Action here  
                                Command = new DelegateCommand<CancelEventArgs>(x => { }, true)
                            };

                            var cancelCommand = new UICommand()
                            {
                                Id = MessageBoxResult.Cancel,
                                Caption = "إلغاء",
                                IsCancel = true,
                                IsDefault = false
                            };
                            PrimaryViewModel customViewModel = PrimaryViewModel.Create();
                            customViewModel.MyTitle = UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault().Name;
                            customViewModel.Value = 0;
                            var result = PrimaryService.ShowDialog(new List<UICommand> { okCommand, cancelCommand }, UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault().Name, customViewModel);

                            if (result.IsDefault)
                            {
                                InvoiceItem invoiceItem = this.Entity.InvoiceItems.Where(x => x.StockItemID == id).FirstOrDefault();
                                //if (invoiceItem == null)
                                //{
                                    invoiceItem = new InvoiceItem();
                                    invoiceItem.UnitPrice = customViewModel.Value;
                                    invoiceItem.Quantity = 1;
                                    invoiceItem.StockItemID = id;
                                    invoiceItem.StockItem = UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault();
                                    this.Entity.InvoiceItems.Add(invoiceItem);
                                //}
                                //else
                                //{
                                //    invoiceItem.UnitPrice += customViewModel.Value;

                                //}
                                base.Save();
                            }
                        }
                        else
                        {
                            InvoiceItem invoiceItem = this.Entity.InvoiceItems.Where(x => x.StockItemID == id).FirstOrDefault();
                            if (invoiceItem == null)
                            {
                                invoiceItem = new InvoiceItem();
                                invoiceItem.Quantity = 1;
                                invoiceItem.MyQuantity = 1;
                                invoiceItem.StockItemID = id;
                                invoiceItem.StockItem = UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault();
                                invoiceItem.UnitPrice = invoiceItem.StockItem.SalePrice;
                                this.Entity.InvoiceItems.Add(invoiceItem);
                            }
                            else
                            {
                                invoiceItem.Quantity += 1;
                                invoiceItem.MyQuantity = invoiceItem.Quantity;

                            }
                            UnitOfWork.StockItems.Where(x => x.StockItemID == id).FirstOrDefault().Quantity -= 1;
                            base.Save();
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

        }

        public void New()
        {
            base.Save();
            this.Entity = CreateEntity();
            base.Save();
        }
        protected override void OnBeforeEntityDeleted(long primaryKey, Invoice entity)
        {
            base.OnBeforeEntityDeleted(primaryKey, entity);
            int itemsCount = this.Entity.InvoiceItems.Count;
            for (int i = 0; i < itemsCount; i++)
            {
                InvoiceItem item = this.Entity.InvoiceItems.ElementAt(0);
                if (!item.StockItem.IsCustom)
                {
                    UnitOfWork.StockItems.Where(x => x.StockItemID == item.StockItemID).First().Quantity += item.Quantity;
                }
                this.Entity.InvoiceItems.Remove(item);
                UnitOfWork.InvoiceItems.Remove(item);

            }
        }
        public void DeleteItem(InvoiceItem InvoiceItem)
        {
            if (!InvoiceItem.StockItem.IsCustom)
            {
                if (InvoiceItem.Quantity >= 1)
                {
                    UnitOfWork.StockItems.Where(x => x.StockItemID == InvoiceItem.StockItemID).First().Quantity += 1;
                    InvoiceItem.Quantity -= 1;
                    InvoiceItem.MyQuantity -= 1;
                }
                else
                {
                    UnitOfWork.StockItems.Where(x => x.StockItemID == InvoiceItem.StockItemID).First().Quantity += InvoiceItem.Quantity;
                    InvoiceItem.Quantity -= InvoiceItem.Quantity;
                }
                InvoiceItem.MyQuantity = InvoiceItem.Quantity;
                if (InvoiceItem.Quantity == 0)
                {
                    this.Entity.InvoiceItems.Remove(InvoiceItem);
                    UnitOfWork.InvoiceItems.Remove(InvoiceItem);
                }
            }
            //IsCustom
            else
            {
                this.Entity.InvoiceItems.Remove(InvoiceItem);
                UnitOfWork.InvoiceItems.Remove(InvoiceItem);
            }
            base.Save();
            this.RaisePropertiesChanged();
        }


        public IEntitiesViewModel<InvoiceItem> LookUpInvoiceItems
        {
            get
            {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.LookUpInvoiceItems,
                    getRepositoryFunc: x => x.InvoiceItems);
            }
        }

        public CollectionViewModelBase<InvoiceItem, InvoiceItem, long, IDBModelUnitOfWork> InvoiceInvoiceItemsDetails
        {
            get
            {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.InvoiceInvoiceItemsDetails,
                    getRepositoryFunc: x => x.InvoiceItems,
                    foreignKeyExpression: x => x.InvoiceID,
                    navigationExpression: x => x.Invoice);
            }
        }

        public void Print()
        {
            try
            {

                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer where Default=True");

                string printerName = "";
                foreach (ManagementObject printer in searcher.Get())
                {
                    printerName = printer["Name"].ToString().ToLower();
                    if ((bool)printer["WorkOffline"])
                    {
                        WinUIMessageBox.Show("Failed To Print", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        return;
                    }

                }

                var doc = new PrintDocument();
                doc.PrintPage += new PrintPageEventHandler(ProvideContent);
                doc.Print();
            }
            catch (Exception)
            {
                WinUIMessageBox.Show("Failed To Print", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }
        public bool CanPrint()
        {
            if (this.Entity == null || this.Entity.InvoiceItems == null || this.Entity.InvoiceItems.Count == 0)
                return false;
            else
                return true;
        }
        public void ProvideContent(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Courier New", 10);

            float fontHeight = font.GetHeight();

            int startX = 0;
            int startY = 0;
            int Offset = 20;

            //e.PageSettings.PaperSize.Width = 50;
            graphics.DrawString("Welcome", new Font("Courier New", 8),
                                new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;

            graphics.DrawString("Invoice NB:" + this.Entity.InvoiceID,
                        new Font("Courier New", 14),
                        new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;

            graphics.DrawString("Date:" + this.Entity.Date.ToString(),
                        new Font("Courier New", 12),
                        new SolidBrush(Color.Black), startX, startY + Offset);

            Offset = Offset + 20;
            string underLine = "------------------------------";

            graphics.DrawString(underLine, new Font("Courier New", 14),
                        new SolidBrush(Color.Black), startX, startY + Offset);

            Offset = Offset + 20;
            decimal tot = 0;

            const int FIRST_COL_PAD = 13;
            const int SECOND_COL_PAD = 4;
            const int THIRD_COL_PAD = 10;

            var sb = new StringBuilder();

            sb.Append("Item".PadRight(FIRST_COL_PAD));
            sb.Append("Qty".PadRight(SECOND_COL_PAD));
            sb.AppendLine("Tot".PadLeft(THIRD_COL_PAD));

            foreach (InvoiceItem item in this.Entity.InvoiceItems)
            {
                decimal itemTotal = (item.Quantity  * item.UnitPrice);
                if (item.StockItem.Name.Length > 12)
                    sb.Append(item.StockItem.Name.Substring(0, 12).ToString().PadRight(FIRST_COL_PAD));
                else
                    sb.Append(item.StockItem.Name.PadRight(FIRST_COL_PAD));
                sb.Append(item.Quantity.ToString().PadRight(SECOND_COL_PAD));

                sb.AppendLine(string.Format("{0:0,0} LL", itemTotal).PadLeft(THIRD_COL_PAD));

                tot += (itemTotal);
            }
            int count = this.Entity.InvoiceItems.Count;

            graphics.DrawString(sb.ToString(), new Font("Courier New", 12),
                       new SolidBrush(Color.Black), startX, startY + Offset);



            Offset = Offset + (20 * count) + 20;
            underLine = "-----------------------------------";
            graphics.DrawString(underLine, new Font("Courier New", 12),
                        new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;

            string Grosstotal = "Total: " + string.Format("{0:0,0} LL", tot);
            graphics.DrawString(Grosstotal, new Font("Courier New", 12),
                        new SolidBrush(Color.Black), startX, startY + Offset);

        }

    }
    public partial class CustomViewModel : SingleObjectViewModel<Invoice, long, IDBModelUnitOfWork>
    {
        public static CustomViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new CustomViewModel(unitOfWorkFactory));
        }

        protected CustomViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Invoices, x => x.InvoiceID)
        {

        }
        public string MyTitle { get; set; }
        protected override string GetTitle()
        {
            return MyTitle;
        }

        public int Value { get; set; }
    }
    public partial class PrimaryViewModel : SingleObjectViewModel<Invoice, long, IDBModelUnitOfWork>
    {
        public static PrimaryViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new PrimaryViewModel(unitOfWorkFactory));
        }

        protected PrimaryViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Invoices, x => x.InvoiceID)
        {

        }
        public string MyTitle { get; set; }
        protected override string GetTitle()
        {
            return MyTitle;
        }

        public int Value { get; set; }
    }
}
