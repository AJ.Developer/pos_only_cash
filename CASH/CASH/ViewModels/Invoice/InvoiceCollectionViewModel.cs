﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using CASH.DBModelDataModel;
using CASH.Common;
using CASH.DataLayer;
using System.Collections.ObjectModel;
using DevExpress.Xpf.WindowsUI;
using System.Drawing;
using System.Text;
using System.Drawing.Printing;
using System.Management;
using DevExpress.Mvvm.ViewModel;
using DevExpress.Mvvm.Native;
using System.Windows;
using DevExpress.Xpf.Core;

namespace CASH.ViewModels
{

    public partial class InvoiceCollectionViewModel : CollectionViewModel<Invoice, long, IDBModelUnitOfWork>
    {
        public override void OnLoaded()
        {
            base.OnLoaded();
            Refresh();
        }
        public static InvoiceCollectionViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
        {
            return ViewModelSource.Create(() => new InvoiceCollectionViewModel(unitOfWorkFactory));
        }

        protected InvoiceCollectionViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Invoices)
        {
            LoadEntities(true);
            
            IsCardView = false;
            DXMessageBoxLocalizer.Active = new MyDXMessageBoxLocalizer();
        }
        public void DeleteAll()
        {
            MessageBoxResult result;
            
            result = WinUIMessageBox.Show("هل أنت متأكّد ؟", "إلغاء كلّ الفواتير ما قبل 24 ساعة", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.Yes)
            {
                DBModel dBModel = new DBModel();
                ObservableCollection<Invoice> OldInvoices = dBModel.Invoices?.ToObservableCollection();
                OldInvoices = OldInvoices.Where(x => (DateTime.Now - x.Date).Value.Days > 0)?.ToObservableCollection();
                dBModel.Invoices.RemoveRange(OldInvoices);
                dBModel.SaveChanges();
                this.Refresh();
            }
            else
            {

            }


        }
        protected override void OnEntitiesAssigned(Func<Invoice> getSelectedEntityCallback)
        {
            base.OnEntitiesAssigned(getSelectedEntityCallback);
            FilteredEntities = this.Entities;
            Last24Hours = Entities?.Where(x => (DateTime.Now - x.Date).Value.Days == 0).ToObservableCollection();
        }
        public virtual ObservableCollection<Invoice> Last24Hours { get; set; }
        public virtual ObservableCollection<Invoice> FilteredEntities { get; set; }
        public virtual bool IsCardView { get; set; }
        public void SwitchView()
        {
            IsCardView = !IsCardView;
        }
        protected override void OnEntitySaved(long primaryKey, Invoice entity)
        {
            base.OnEntitySaved(primaryKey, entity);
            Refresh();
        }
        public override void Delete(Invoice projectionEntity)
        {
            int itemsCount = this.SelectedEntity.InvoiceItems.Count;
            for (int i = 0; i < itemsCount; i++)
            {
                
                InvoiceItem item = this.SelectedEntity.InvoiceItems.ElementAt(i);
                if (!item.StockItem.IsCustom)
                {
                    item.StockItem.Quantity += item.Quantity;
                }
               
            }
            base.Delete(projectionEntity);
        }

        public void Print()
        {
            try
            {

                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer where Default=True");

                string printerName = "";
                foreach (ManagementObject printer in searcher.Get())
                {
                    printerName = printer["Name"].ToString().ToLower();
                    if ((bool)printer["WorkOffline"])
                    {
                        WinUIMessageBox.Show("Failed To Print", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        return;
                    }
                   
                }

                var doc = new PrintDocument();
                doc.PrintPage += new PrintPageEventHandler(ProvideContent);
                doc.Print();
            }
            catch (Exception)
            {
                WinUIMessageBox.Show("Failed To Print", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }
        public bool CanPrint()
        {
            if (this.SelectedEntity == null || this.SelectedEntity.InvoiceItems == null || this.SelectedEntity.InvoiceItems.Count == 0)
                return false;
            else
                return true;
        }
        public void ProvideContent(object sender, PrintPageEventArgs e)
        {
            try
            {
                Graphics graphics = e.Graphics;
                Font font = new Font("Courier New", 10);

                float fontHeight = font.GetHeight();

                int startX = 0;
                int startY = 0;
                int Offset = 20;

                //e.PageSettings.PaperSize.Width = 50;
                graphics.DrawString("Welcome", new Font("Courier New", 8),
                                    new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 20;

                graphics.DrawString("Invoice NB:" + this.SelectedEntity.InvoiceID,
                            new Font("Courier New", 14),
                            new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 20;

                graphics.DrawString("Date:" + this.SelectedEntity.Date.ToString(),
                            new Font("Courier New", 12),
                            new SolidBrush(Color.Black), startX, startY + Offset);

                Offset = Offset + 20;
                string underLine = "------------------------------";

                graphics.DrawString(underLine, new Font("Courier New", 14),
                            new SolidBrush(Color.Black), startX, startY + Offset);

                Offset = Offset + 20;
                decimal tot = 0;

                const int FIRST_COL_PAD = 13;
                const int SECOND_COL_PAD = 4;
                const int THIRD_COL_PAD = 10;

                var sb = new StringBuilder();

                sb.Append("Item".PadRight(FIRST_COL_PAD));
                sb.Append("Qty".PadRight(SECOND_COL_PAD));
                sb.AppendLine("Tot".PadLeft(THIRD_COL_PAD));

                foreach (InvoiceItem item in this.SelectedEntity.InvoiceItems)
                {
                    decimal itemTotal = (item.Quantity* item.UnitPrice);
                    if (item.StockItem.Name.Length > 12)
                        sb.Append(item.StockItem.Name.Substring(0, 12).ToString().PadRight(FIRST_COL_PAD));
                    else
                        sb.Append(item.StockItem.Name.PadRight(FIRST_COL_PAD));
                    sb.Append(item.Quantity.ToString().PadRight(SECOND_COL_PAD));

                    sb.AppendLine(string.Format("{0:0,0} LL", itemTotal).PadLeft(THIRD_COL_PAD));

                    tot += (itemTotal);
                }
                int count = this.SelectedEntity.InvoiceItems.Count;

                graphics.DrawString(sb.ToString(), new Font("Courier New", 12),
                           new SolidBrush(Color.Black), startX, startY + Offset);



                Offset = Offset + (20 * count) + 20;
                underLine = "-----------------------------------";
                graphics.DrawString(underLine, new Font("Courier New", 12),
                            new SolidBrush(Color.Black), startX, startY + Offset);
                Offset = Offset + 20;

                string Grosstotal = "Total: " + string.Format("{0:0,0} LL", tot);
                graphics.DrawString(Grosstotal, new Font("Courier New", 12),
                            new SolidBrush(Color.Black), startX, startY + Offset);
            }
            catch (Exception)
            {
                WinUIMessageBox.Show("Failed To Print", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }

    }
    public class Filter
    {

        public virtual string Name { get; set; }
        public virtual string URL { get; set; }
    }
    public class MyDXMessageBoxLocalizer : DXMessageBoxLocalizer
    {
        protected override void PopulateStringTable()
        {
            base.PopulateStringTable();
            this.AddString(DXMessageBoxStringId.Yes, "نعم");
            this.AddString(DXMessageBoxStringId.No, "كلّا");
            this.AddString(DXMessageBoxStringId.Ok, "حسنا");
        }
    }





}